<?php
/*
Template Name: Front
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lives Need Saving - Volunteer Today</title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css">

<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie7.css">
<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie8.css">
<![endif]-->

</head>

<body id="homepage">
	
	<?php wp_link_pages(); ?>
	
<div id="header">
	<h1 id="logo"><a href="#home">Volunteer Rescue Squads of Virginia Beach</a></h1>
	<div id="nav">
		<ul>
			<li><a href="#joinus-page" class="btn-slide btn-joinus">Join Us</a></li>
			<li><a href="#contact-page" class="btn-slide btn-contact">Contact Us</a></li>
			<li><a href="#questions-page" class="btn-slide btn-questions">Questions</a></li>
			<li><a href="<?php echo site_url(); ?>/blog">VBRS Blog</a></li>
		</ul>
	</div>
	<div id="socialmedia">
		<span>Follow</span>
		<ul>
			<li class="rss"><a href="http://livesneedsaving.org/blog/?feed=rss2" target="_blank">Subscribe to our RSS Feed</a></li>
			<li class="facebook"><a href="http://www.facebook.com/livesneedsaving" target="_blank">Follow us on Facebook</a></li>
			<li class="youtube"><a href="http://www.youtube.com/user/VBRSFOUNDATION" target="_blank">View our YouTube Channel</a></li>
			<li class="addthis"><!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style "><a class="addthis_button_compact"></a></div>
		<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4fa2c2262676f0c4"></script>
		<!-- AddThis Button END --></li>
		</ul>
	</div>
</div>
	<div id="pxs_container" class="pxs_container">
		<div class="pxs_bg">
			<div class="pxs_bg1"></div>
			<div class="pxs_bg2"></div>
		</div>
	
		<div class="pxs_slider_wrapper">
			<ul class="pxs_slider">
				<li id="jay">
					<div>
						<h2>Life's a classroom.</h2>
						<p class="type">Name:</p>
						<p>Jay Leach
						 <a href="#jaymore" class="fancybox more">More About...</a>
						<a href="#jaysaved" class="fancybox saved">The Lives Saved</a></p>
						<span id="jayvid"><iframe class="emsvid" src="http://www.youtube.com/embed/xgp78VCLfRI?showinfo=0&autohide=1&rel=0&theme=dark&wmode=transparent&enablejsapi=1" frameborder="0" allowfullscreen></iframe></span>
						<span class="leftColumn">
							<p class="type">Position:</p>
							<p>Paramedic</p>
							<p class="type">Join Year:</p>
							<p>1989</p>
						</span>
						<span class="rightColumn">
							<p class="type">Occupation:</p>
							<p>School Teacher</p>
							<p class="type">Rescue Station:</p>
							<p>Station 14 Virginia Beach</p>
						</span>
					</div>
				</li>
				<li id="judy">
					<div>
						<h2>The Sewing Can Wait.</h2>
							<p class="type">Name:</p>
							<p>Judy Jackson
							 <a href="#judymore" class="fancybox more">More About...</a>
							<a href="#judysaved" class="fancybox saved">The Lives Saved</a></p>
						<span id="judyvid"><iframe class="emsvid" src="http://www.youtube.com/embed/Lir6v_oV8XQ?showinfo=0&autohide=1&rel=0&theme=dark&wmode=transparent&enablejsapi=1" frameborder="0" allowfullscreen></iframe></span>
						<span class="leftColumn">
							<p class="type">Position:</p>
							<p>Paramedic</p>
							<p class="type">Join Year:</p>
							<p>2001</p>
						</span>
						<span class="rightColumn">
							<p class="type">Occupation:</p>
							<p>Homemaker</p>
							<p class="type">Rescue Station:</p>
							<p>Station 14 Virginia Beach</p>
						</span>
					</div>
				</li>
				<li id="gary">
					<div>
						<h2>Same Passion<br />Different Uniform.</h2>
							<p class="type">Name:</p>
							<p>Gary Wilks
							 <a href="#garymore" class="fancybox more">More About...</a>
							<a href="#garysaved" class="fancybox saved">The Lives Saved</a></p>
						<span id="garyvid"><iframe class="emsvid" src="http://www.youtube.com/embed/7T3EEOz34RY?showinfo=0&autohide=1&rel=0&theme=dark&wmode=transparent&enablejsapi=1" frameborder="0" allowfullscreen></iframe></span>
						<span class="leftColumn">
							<p class="type">Position:</p>
							<p>EMT</p>
							<p class="type">Join Year:</p>
							<p>1997</p>
						</span>
						<span class="rightColumn">
							<p class="type">Occupation:</p>
							<p>Retired United States Navy</p>
							<p class="type">Rescue Station:</p>
							<p>Station 5 P.A. Courthouse</p>
						</span>
					</div>
				</li>
			</ul>
			<div class="pxs_navigation">
				<span class="pxs_next"></span>
				<span class="pxs_prev"></span>
			</div>
			<ul class="pxs_thumbnails">
				<li></li>
				<li></li>
				<li></li>
			</ul>
		</div>
	</div>
	
<div id="panel">
		<div id="blipborder"></div>
		<div id="bottomcontainer">

			<h2 class="panel-slogan">Lives Need Saving.<br>
			Start With Yours.</h2>
			
			
			<div id="pxs_container2" class="pxs_container">
	
				<div class="pxs_slider_wrapper">
					<ul class="pxs_slider pxs_slider2">
						<li id="joinus">
								<?php 
									$page_id = 6;
									$page_data = get_page( $page_id );
									echo '<h3>'. $page_data->post_title .'</h3>';
									echo apply_filters('the_content', $page_data->post_content);
								?>
						</li>
						<li id="contact">
							<div class="leftColumn">
								<?php 
									$page_id = 8;
									$page_data = get_page( $page_id );
									echo '<h3>'. $page_data->post_title .'</h3>';
									echo apply_filters('the_content', $page_data->post_content);
								?>
							</div>
							<div class="rightColumn">
								<?php echo do_shortcode( '[contact-form-7 id="39" title="Contact Us"]' ); ?>
							</div>
							
						</li>
						<li id="questions">
							<div class="oneColumn">
								<?php 
									$page_id = 11;
									$page_data = get_page( $page_id );
									echo '<h3>'. $page_data->post_title .'</h3>';
									echo apply_filters('the_content', $page_data->post_content);
								?>
							</div>
							<div class="rightColumn"></div>
						</li>
					</ul>
					<div class="pxs_navigation">
						<span class="pxs_next"></span>
						<span class="pxs_prev"></span>
					</div>
				</div>
			</div>
			
		</div>
</div>


<!-- JAY -->
<div id="jaymore" class="fancyboxclipboard">
<h2>Jay Leach</h2>
<?php 
	$page_id = 13;
	$page_data = get_page( $page_id );
	echo apply_filters('the_content', $page_data->post_content);
?>
<span class="fancybox-close backto" title="Close">Back to Jay</span>
</div>
<div id="jaysaved" class="fancyboxclipboard savedclip">
<h2>Jay Leach</h2>
<?php 
	$page_id = 15;
	$page_data = get_page( $page_id );
?>
<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
<?php echo apply_filters('the_content', $page_data->post_content); ?>
<span class="fancybox-close backto" title="Close">Back to Jay</span>
</div>

<!-- JUDY -->
<div id="judymore" class="fancyboxclipboard">
<h2>Judy Jackson</h2>
<?php 
	$page_id = 18;
	$page_data = get_page( $page_id );
	echo apply_filters('the_content', $page_data->post_content);
?>
<span class="fancybox-close backto" title="Close">Back to Judy</span>
</div>
<div id="judysaved" class="fancyboxclipboard savedclip">
<h2>Judy Jackson</h2>
<?php 
	$page_id = 20;
	$page_data = get_page( $page_id );
?>
<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
<?php echo apply_filters('the_content', $page_data->post_content); ?>
<span class="fancybox-close backto" title="Close">Back to Judy</span>
</div>

<!-- GARY -->
<div id="garymore" class="fancyboxclipboard">
<h2>Gary Wilks</h2>
<?php 
	$page_id = 22;
	$page_data = get_page( $page_id );
	echo apply_filters('the_content', $page_data->post_content);
?>
<span class="fancybox-close backto" title="Close">Back to Gary</span>
</div>
<div id="garysaved" class="fancyboxclipboard savedclip">
<h2>Gary Wilks</h2>
<?php 
	$page_id = 24;
	$page_data = get_page( $page_id );
?>
<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
<?php echo apply_filters('the_content', $page_data->post_content); ?>
<span class="fancybox-close backto" title="Close">Back to Gary</span>
</div>



<footer>
	Copyright <?php echo date("Y"); ?> - The Virginia Beach Rescue Squad Foundation
</footer>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plx.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.quick.pagination.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
<script type='text/javascript' src="<?php echo get_site_url(); ?>/wp-content/plugins/contact-form-7/jquery.form.js?ver=3.08"></script>
<script type='text/javascript' src="<?php echo get_site_url(); ?>/wp-content/plugins/contact-form-7/scripts.js?ver=3.1.2"></script>

<script type="text/javascript">
	$(function() {
		$('#pxs_container').parallaxSlider();
		$('#pxs_container2').parallaxSlider();
		$("ul.pagination").quickPagination({pageSize:"1"});
		$('.backto').live('click',function(){
		    $.fancybox.close();
		    return false;
		});
	});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1652181-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>


