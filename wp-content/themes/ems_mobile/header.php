<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="author" content="BCF - Boom Your Brand" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />
<meta name="viewport" content="width=device-width">

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?><?php if (get_bloginfo('description', 'display')) { echo " - ".get_bloginfo('description', 'display'); } ?></title>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_enqueue_script('jquery'); ?>
<script type="text/javascript" charset="utf-8">
	
	window.addEventListener("load",function() {
	  // Set a timeout...
	  setTimeout(function(){
	    // Hide the address bar!
	    window.scrollTo(0, 1);
	  }, 0);
	});

</script>

<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>

<div id="wrap">
	
	<div id="header" class="blog">
		<div class="blogwrapper">
			<h1 id="logo"><a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a></h1>
			<div id="nav">
				<ul>
					<li><a href="<?php echo site_url(); ?>/blog">VBRS Blog</a></li>
					<li><a href="<?php echo site_url(); ?>/">Back to Main Site</a></li>
				</ul>
			</div>
			<div id="search">
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
	
