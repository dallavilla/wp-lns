<?php
/*
Template Name: Front
*/
?>

<!DOCTYPE html>
<html>
<head>
<title>Lives Need Saving</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="BCF - Boom Your Brand" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/mobile.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/jqtouch/jqtouch.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/themes/jqt/theme.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/extensions/jqt.bars/jqt.bars.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/jqtouch/jqtouch.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/extensions/jqt.bars/themes/jqt/theme.css">

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jqtouch/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jqtouch/jqtouch.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/extensions/jqt.bars/iscroll-min.js" type="application/x-javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">

	var jQT = new $.jQTouch({
	  icon: 'jqtouch.png',
	  addGlossToIcon: true,
	  startupScreen: 'jqt_startup.png',
	  statusBar: 'black-translucent',
	  fullScreen: true,
	  preloadImages: []	
	});

    $(document).ready(function () {

    var hCarousel;

    function initCarousel() {


		var hCarousel = new iScroll('carouselWrapper', {
			
			momentum: false,
			snap: true,
			hScrollbar: false,
			hScrollbar: false,
			onScrollEnd: function () {
				document.querySelector('#carouselIndicator > li.focus').className = '';
				document.querySelector('#carouselIndicator > li:nth-child(' +
				(this.currPageX+1) + ')').className = 'focus';
	
			}
	
		});
    }

    initCarousel();
    });
	
	window.addEventListener("load",function() {
	  // Set a timeout...
	  setTimeout(function(){
	    // Hide the address bar!
	    window.scrollTo(0, 1);
	  }, 0);
	});

</script>

</head>
<body id="jqt">
<div id="home" class="current">
	<div id="carouselHeader">
		<h1 id="logo"><a href="#home">Volunteer Rescue Squads of Virginia Beach</a></h1>
		<p class="nav"><a href="#join" class="slideup">Join Us</a>|<a href="#contact" class="slideup">Contact Us</a>|<a href="#questions" class="slideup">Questions</a>|<a href="<?php echo site_url(); ?>/blog" target="_webapp">Blog</a></p>
	</div>
	<div id="carouselWrapper" snap="true">
		<div id="carouselScroller">
			<ul>
				<li class="jay">
					<div>
						<h2>Life's a classroom.</h2>
						<span class="leftColumn">
							
						</span>
						<span class="rightColumn">
							<p class="type">Name:</p>
							<p>Jay Leach</p>
							<p><a href="#jaymore" class="more flip">More About...</a></p>
							<p><a href="#jaysaved" class="saved flip">The Lives Saved</a></p>
							<p class="type">Position:</p>
							<p>Paramedic</p>
							<p class="type">Join Year:</p>
							<p>1989</p>
							<p class="type">Occupation:</p>
							<p>School Teacher</p>
							<p class="type">Rescue Station:</p>
							<p>Station 14<br/>Virginia Beach</p>
							<p><a class="saved" href="http://www.youtube.com/embed/xgp78VCLfRI" target="_webapp">Watch His Story</a></p>
						</span>
					</div>
				</li>
				<li class="judy">
					<div>
						<h2>The Sewing Can Wait.</h2>
						<span class="leftColumn">
						</span>
						<span class="rightColumn">
							<p class="type">Name:</p>
							<p>Judy Jackson</p>
							<p><a href="#judymore" class="more">More About...</a></p>
							<p><a href="#judysaved" class="saved">The Lives Saved</a></p>
							<p class="type">Position:</p>
							<p>Paramedic</p>
							<p class="type">Join Year:</p>
							<p>2001</p>
							<p class="type">Occupation:</p>
							<p>Homemaker</p>
							<p class="type">Rescue Station:</p>
							<p>Station 14<br/>Virginia Beach</p>
							<p><a class="saved" href="http://www.youtube.com/embed/Lir6v_oV8XQ" target="_webapp">Watch Her Story</a></p>
						</span>
					</div>
				</li>
				<li class="gary">
					<div>
						<h2>Same Passion<br />Different Uniform.</h2>
						<span class="leftColumn">
						</span>
						<span class="rightColumn">
							<p class="type">Name:</p>
							<p>Gary Wilks</p>
							<p><a href="#garymore" class="more">More About...</a></p>
							<p><a href="#garysaved" class="saved">The Lives Saved</a></p>
							<p class="type">Position:</p>
							<p>EMT</p>
							<p class="type">Join Year:</p>
							<p>1997</p>
							<p class="type">Occupation:</p>
							<p>Retired Navy</p>
							<p class="type">Rescue Station:</p>
							<p>Station 5<br/>P.A. Courthouse</p>
							<p><a class="saved" href="http://www.youtube.com/embed/7T3EEOz34RY" target="_webapp">Watch His Story</a></p>
						</span>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div id="carouselNav">
		<div class="center">
			<ul id="carouselIndicator">
				<li class="focus">1</li>
				<li>2</li>
				<li>3</li>
			</ul>
		</div>
	</div>
</div>

<!-- Bottom Pages -->
<div id="join" class="interior">
	<div class="toolbar"> <a class="button back flipright" href="#">Back</a>
		<h1>Join Us</h1>
    </div>
	<div class="p">
		<?php 
			$page_id = 6;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
		<br/><br/>
	</div>
</div>

<div id="contact" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Contact Us</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 8;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
		<?php echo do_shortcode( '[contact-form-7 id="39" title="Contact Us"]' ); ?>
		<br/><br/>
	</div>
</div>

<div id="questions" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Questions</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 11;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
	</div>
</div>

<div id="jaymore" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Jay Leach</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 13;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
	</div>
</div>

<div id="jaysaved" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Jay Leach</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 15;
			$page_data = get_page( $page_id );
		?>
		<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
		<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
		<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
		<?php echo apply_filters('the_content', $page_data->post_content); ?>
	</div>
</div>

<div id="judymore" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Judy Jackson</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 18;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
	</div>
</div>

<div id="judysaved" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Judy Jackson</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 20;
			$page_data = get_page( $page_id );
		?>
		<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
		<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
		<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
		<?php echo apply_filters('the_content', $page_data->post_content); ?>
	</div>
</div>

<div id="garymore" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Gary Wilks</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 22;
			$page_data = get_page( $page_id );
			echo apply_filters('the_content', $page_data->post_content);
		?>
	</div>
</div>

<div id="garysaved" class="interior">
	<div class="toolbar"> <a class="button back" href="#">Back</a>
		<h1>Gary Wilks</h1>
	</div>
	<div class="p">
		<?php 
			$page_id = 24;
			$page_data = get_page( $page_id );
		?>
		<span class="life"><?php echo get_post_meta($page_id, 'Life', true); ?></span>
		<span class="rescuedate"><?php echo get_post_meta($page_id, 'Rescue Date', true); ?></span>
		<span class="incident"><?php echo get_post_meta($page_id, 'Incident', true); ?></span>
		<?php echo apply_filters('the_content', $page_data->post_content); ?>
	</div>
</div>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1652181-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>

