(function($) {
	
	$.fn.parallaxSlider = function(options) {
		var opts = $.extend({}, $.fn.parallaxSlider.defaults, options);
		return this.each(function() {
			var $pxs_container 	= $(this),
			o 				= $.meta ? $.extend({}, opts, $pxs_container.data()) : opts;
			
			//the main slider
			var $pxs_slider		= $('.pxs_slider',$pxs_container),
			//the elements in the slider
			$elems			= $pxs_slider.children(),
			//total number of elements
			total_elems		= $elems.length,
			//the navigation buttons
			$pxs_next		= $('.pxs_next',$pxs_container),
			$pxs_prev		= $('.pxs_prev',$pxs_container),
			//the bg images
			$pxs_bg1		= $('.pxs_bg1',$pxs_container),
			$pxs_bg2		= $('.pxs_bg2',$pxs_container),
			//current image
			current			= 0,
			//the thumbs container
			$pxs_thumbnails = $('.pxs_thumbnails',$pxs_container),
			//the thumbs
			$thumbs			= $pxs_thumbnails.children(),
			//the interval for the autoplay mode
			slideshow,
			$pxs_slider_wrapper = $('.pxs_slider_wrapper',$pxs_container);
				
			//first preload all the images
			var loaded		= 0,
			$images		= $pxs_slider_wrapper.find('div');
				
			$images.each(function(){
				$pxs_slider_wrapper.show();
					
				//one images width (assuming all images have the same sizes)
				var one_image_w = 960;
				if (document.body && document.body.offsetWidth) {
				 one_image_w = document.body.offsetWidth;
				}
				if (document.compatMode=='CSS1Compat' &&
				    document.documentElement &&
				    document.documentElement.offsetWidth ) {
				 one_image_w = document.documentElement.offsetWidth;
				}
				if (window.innerWidth) {
				 one_image_w = window.innerWidth;
				}
				
		
				/*
				need to set width of the slider,
				of each one of its elements, and of the
				navigation buttons
				 */

				setWidths($pxs_slider,$elems,total_elems,$pxs_bg1,$pxs_bg2,one_image_w,$pxs_next,$pxs_prev);
				
					
				//make the first thumb be selected
				highlight($thumbs.eq(0));
					
				//slide when clicking the navigation buttons
				$pxs_next.bind('click',function(){
					++current;
					stopPlayers();
					if(current >= total_elems)
						if(o.circular)
							current = 0;
					else{
						--current;
						return false;
					}
					highlight($thumbs.eq(current));
					slide(current,
					$pxs_slider,
					$pxs_bg2,
					$pxs_bg1,
					o.speed,
					o.easing,
					o.easingBg);
				});
				$pxs_prev.bind('click',function(){
					--current;
					stopPlayers();
					if(current < 0)
						if(o.circular)
							current = total_elems - 1;
					else{
						++current;
						return false;
					}
					highlight($thumbs.eq(current));
					slide(current,
					$pxs_slider,
					$pxs_bg2,
					$pxs_bg1,
					o.speed,
					o.easing,
					o.easingBg);
				});
		
				/*
				clicking a thumb will slide to the respective image
				 */
				$thumbs.bind('click',function(){
					var $thumb	= $(this);
					stopPlayers();
					highlight($thumb);
					//if autoplay interrupt when user clicks
					if(o.auto)
						clearInterval(slideshow);
					current 	= $thumb.index();
					slide(current,
					$pxs_slider,
					$pxs_bg2,
					$pxs_bg1,
					o.speed,
					o.easing,
					o.easingBg);
				});
		
			
		
				/*
				activate the autoplay mode if
				that option was specified
				 */
				if(o.auto != 0){
					o.circular	= true;
					slideshow	= setInterval(function(){
						$pxs_next.trigger('click');
					},o.auto);
				}
		
				/*
				when resizing the window,
				we need to recalculate the widths of the
				slider elements, based on the new windows width.
				we need to slide again to the current one,
				since the left of the slider is no longer correct
				 */
				$(window).resize(function(){
					w_w	= window.innerWidth;
					setWidths($pxs_slider,$elems,total_elems,$pxs_bg1,$pxs_bg2,one_image_w,$pxs_next,$pxs_prev);
					slide(current,
					$pxs_slider,
					$pxs_bg2,
					$pxs_bg1,
					1,
					o.easing,
					o.easingBg);
				});
				
				$(".btn-joinus").click(function(){
					current = 0;
					var slide_to = parseInt(-w_w * current);
					$(".pxs_slider2").stop().animate({left : slide_to + 'px'}, 1000, 'jswing');
					$(".pxs_thumbnails").css('margin-bottom', '-200px');
					stopPlayers();
				});
				$(".btn-contact").click(function(){
					current = 1;
					var slide_to = parseInt(-w_w * current);
					$(".pxs_slider2").stop().animate({left : slide_to + 'px'}, 1000, 'jswing');
					$(".pxs_thumbnails").css('margin-bottom', '-200px');
					stopPlayers();
				});
				$(".btn-questions").click(function(){
					current = 2;
					var slide_to = parseInt(-w_w * current);
					$(".pxs_slider2").stop().animate({left : slide_to + 'px'}, 1000, 'jswing');
					$(".pxs_thumbnails").css('margin-bottom', '-200px');
					stopPlayers();
				});

			}).error(function(){
				alert('here')
			});
		});
	};
	
	//the current windows width
	var w_w = 960;
	if (document.body && document.body.offsetWidth) {
	 w_w = document.body.offsetWidth;
	}
	if (document.compatMode=='CSS1Compat' &&
	    document.documentElement &&
	    document.documentElement.offsetWidth ) {
	 w_w = document.documentElement.offsetWidth;
	}
	if (window.innerWidth) {
	 w_w = window.innerWidth;
	}
	
	var slide			= function(current,
	$pxs_slider,
	$pxs_bg2,
	$pxs_bg1,
	speed,
	easing,
	easingBg){
		var slide_to	= parseInt(-w_w * current);
		$pxs_slider.stop().animate({
			left	: slide_to + 'px'
		},speed, easing);
		$pxs_bg2.stop().animate({
			left	: slide_to*.75 + 'px'
		},speed, easingBg);
		$pxs_bg1.stop().animate({
			left	: slide_to*.5 + 'px'
		},speed, easingBg);
	}
	
	var highlight		= function($elem){
		$elem.siblings().removeClass('selected');
		$elem.addClass('selected');
	}
	
	var setWidths		= function($pxs_slider,
	$elems,
	total_elems,
	$pxs_bg1,
	$pxs_bg2,
	one_image_w,
	$pxs_next,
	$pxs_prev){
		/*
		the width of the slider is the windows width
		times the total number of elements in the slider
		 */
		var pxs_slider_w	= w_w * total_elems;
		$pxs_slider.width(pxs_slider_w + 'px');
		//each element will have a width = windows width
		$elems.width(w_w + 'px');
		/*
		we also set the width of each bg image div.
		The value is the same calculated for the pxs_slider
		 */
		// $pxs_bg1.width(w_w*3* + 'px');
		// $pxs_bg2.width(w_w*3* + 'px');
		$pxs_bg1.width(pxs_slider_w + 'px');
		$pxs_bg2.width(pxs_slider_w + 'px');
		
		
		/*
		both the right and left of the
		navigation next and previous buttons will be:
		windowWidth/2 - imgWidth/2 + some margin (not to touch the image borders)
		 */
		// var position_nav	= w_w/2 - one_image_w/2 + 3;
		// 		$pxs_next.css('right', position_nav + 'px');
		// 		$pxs_prev.css('left', position_nav + 'px');
	}
	
	$.fn.parallaxSlider.defaults = {
		auto			: 0,	//how many seconds to periodically slide the content.
								//If set to 0 then autoplay is turned off.
		speed			: 1000,//speed of each slide animation
		easing			: 'jswing',//easing effect for the slide animation
		easingBg		: 'jswing',//easing effect for the background animation
		circular		: true,//circular slider
		thumbRotation	: false//the thumbs will be randomly rotated
	};
	//easeInOutExpo,easeInBack
	
})(jQuery);

//font resize
var $body = $('body'); //Cache this for performance

	var setBodyScale = function() {
		var scaleSource = $body.width(),
			scaleFactor = 0.057,                     
			maxScale = 600,
			minScale = 30; //Tweak these values to taste

		var fontSize = scaleSource * scaleFactor; //Multiply the width of the body by the scaling factor:

		if (fontSize > maxScale) fontSize = maxScale;
		if (fontSize < minScale) fontSize = minScale; //Enforce the minimum and maximums

		$('body').css('font-size', fontSize + '%');
	}

	$(window).resize(function(){
		setBodyScale();
	});

	//Fire it when the page first loads:
	setBodyScale();

	//sliding pane
	$("#logo").click(function() {
		$("#panel").animate({top: "100%"}, 750, 'jswing');
		$(".pxs_thumbnails").css('margin-bottom', '0px');
	});
	$(".btn-slide").click(function(){
		$("#panel").animate({top: "0"}, 1000, 'jswing'),
		$("#bottomcontainer").animate({top: "50%"}, 1000, 'jswing'),
		$(this).toggleClass("active"); return false;
	});


	function callPlayer(frame_id, func, args){
	    if(!frame_id) return;
	    if(frame_id.id) frame_id = frame_id.id;
	    else if(typeof jQuery != "undefined" && frame_id instanceof jQuery && frame_id.length) frame_id = frame_id.get(0).id;
	    if(!document.getElementById(frame_id)) return;
	    args = args || [];

	    /*Searches the document for the IFRAME with id=frame_id*/
	    var all_iframes = document.getElementsByTagName("iframe");
	    for(var i=0, len=all_iframes.length; i<len; i++){
	        if(all_iframes[i].id == frame_id || all_iframes[i].parentNode.id == frame_id){
	           /*The index of the IFRAME element equals the index of the iframe in
	             the frames object (<frame> . */
	           window.frames[i].postMessage(JSON.stringify({
	                "event": "command",
	                "func": func,
	                "args": args,
	                "id": frame_id
	            }), "*");
	        }
	    }
	}
	
	function stopPlayers(){
		callPlayer("jayvid", "stopVideo");
		callPlayer("judyvid", "stopVideo");
		callPlayer("garyvid", "stopVideo");
	}
	
	
//fancybox
$(".fancybox").fancybox();

//turn off ipad scroll

document.ontouchmove = function(e){
             e.preventDefault();
}