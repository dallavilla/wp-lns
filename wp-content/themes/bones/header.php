<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8 oldie"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;
		wp_title( '-', true, 'right' );
		// Add the blog name.
		bloginfo( 'name' );
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' - ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
		?></title>
		
		<meta name="description" content="">
		<meta name="author" content="">
		
		<!-- icons & favicons (for more: http://themble.com/support/adding-icons-favicons/) -->
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

		<!-- default stylesheet -->
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/normalize.css">	
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/blog.css">
		<link rel="stylesheet" media="print" href="<?php echo get_template_directory_uri(); ?>/css/print.css">
		
		<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if necessary -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script>window.jQuery || document.write(unescape('%3Cscript src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery-1.7.1.min.js"%3E%3C/script%3E'))</script>
		
		<!-- drop Google Analytics Here -->
		<!-- end analytics -->
		
		<!-- modernizr -->
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.full.min.js"></script>
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		
		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->
		
		<?php if ( is_page('82') ) { ?>
			<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
			</script>
			<script type="text/javascript">
				var mapPoints = new Array();
				var pointInfo = new Array();

				var image = new google.maps.MarkerImage('/wp-content/themes/bones/images/icon_ems_32.png',
					new google.maps.Size(32,32),
					new google.maps.Point(0,0),
					new google.maps.Point(16,16));
			
				var shadow = new google.maps.MarkerImage('/wp-content/themes/bones/images/icon_ems_shadow.png',
					new google.maps.Size(32,16),
					new google.maps.Point(0,0),
					new google.maps.Point(11,0));
			
				var image2 = new google.maps.MarkerImage('/wp-content/themes/bones/images/icon_ems2_32.png',
					new google.maps.Size(32,32),
					new google.maps.Point(0,0),
					new google.maps.Point(16,16));
			
				var shadow2 = new google.maps.MarkerImage('/wp-content/themes/bones/images/icon_ems2_shadow.png',
					new google.maps.Size(32,16),
					new google.maps.Point(0,0),
					new google.maps.Point(11,0));

				var stations = [
				  ['Ocean Park Volunteer Rescue Squad', -76.102874, 36.907473, 1, '<b>Ocean Park Volunteer Rescue Squad</b><br>Rescue Station 1<br>3769 East Stratford Road<br>Virginia Beach, VA 23455<br>Phone: 464-0594<br><a href="http://www.opvrs.com/" target="_blank">http://www.opvrs.com/</a>',image,shadow],
				  ['Davis Corner Volunteer Rescue Squad', -76.138601, 36.874364, 2, '<b>Davis Corner Volunteer Rescue Squad</b><br>Rescue Station 2<br>4672 Haygood Road<br>Virginia Beach, VA 23455<br>Phone: 460-7574<br><a href="http://www.dcvrs.org/" target="_blank">http://www.dcvrs.org/</a>',image,shadow],
				  ['Chesapeake Beach Volunteer Rescue Squad', -76.131939, 36.913426, 3, '<b>Chesapeake Beach Volunteer Rescue Squad</b><br>Rescue Station 4<br>2444 Pleasure House Road<br>Virginia Beach, VA 23455<br>Phone: 460-7509<br><a href="http://www.rescue4.org/" target="_blank">http://www.rescue4.org/</a>',image,shadow],
				  ['Princess Anne Courthouse Volunteer Rescue Squad', -76.056274, 36.753125, 4, '<b>Princess Anne Courthouse Volunteer Rescue Squad</b><br>Rescue Station 5<br>2461 Princess Anne Road<br>Virginia Beach, VA 23456<br>Phone: 385-4688<br><a href="http://www.pachvrs.org/" target="_blank">http://www.pachvrs.org/</a>',image,shadow],
				  ['Creeds Volunteer Rescue Squad', -76.026957, 36.603479, 5, '<b>Creeds Volunteer Rescue Squad</b><br>Rescue Station 6<br>595 Princess Anne Road<br>Virginia Beach, VA 23457<br>Phone: 721-6389<br><a href="http://www.vbgov.com/" target="_blank">http://www.vbgov.com/</a>',image,shadow],
				  ['Virginia Beach Volunteer Rescue Squad', -76.033912, 36.865805, 6, '<b>Virginia Beach Volunteer Rescue Squad</b><br>Rescue Station 8<br>1243 Bayne Drive<br>Virginia Beach, VA 23454<br>Phone: 333-3365<br><a href="http://www.rescue14.com/" target="_blank">http://www.rescue14.com/</a>',image,shadow],
				  ['Kempsville Volunteer Rescue Squad', -76.159222, 36.827519, 7, '<b>Kempsville Volunteer Rescue Squad</b><br>Rescue Station 9<br>5145 Ruritan Court<br>Virginia Beach, VA 23462<br>Phone: 340-5877<br><a href="http://kvrs.org/" target="_blank">http://kvrs.org/</a>',image,shadow],
				  ['Blackwater Volunteer Rescue Squad', -76.083418, 36.585050, 8, '<b>Blackwater Volunteer Rescue Squad</b><br>Rescue Station 13<br>6009 Blackwater Road<br>Virginia Beach, VA 23457<br>Phone: 421-2200<br><a href="http://www.vbgov.com/" target="_blank">http://www.vbgov.com/</a>',image,shadow],
				  ['Virginia Beach Volunteer Rescue Squad', -75.984052, 36.844156, 9, '<b>Virginia Beach Volunteer Rescue Squad</b><br>Rescue Station 14<br>740 Virginia Beach Boulevard<br>Virginia Beach, VA 23451<br>Phone: 437-4830<br><a href="http://www.rescue14.com/" target="_blank">http://www.rescue14.com/</a>',image,shadow],
				  ['Plaza Volunteer Rescue Squad', -76.096582, 36.829868, 10, '<b>Plaza Volunteer Rescue Squad</b><br>Rescue Station 16<br>3610 South Plaza Trail<br>Virginia Beach, VA 23452<br>Phone: 385-2684<br><a href="http://www.plazarescue.org/" target="_blank">http://www.plazarescue.org/</a>',image,shadow],
				  ['Sandbridge Volunteer Rescue Squad', -75.945666, 36.745778, 11, '<b>Sandbridge Volunteer Rescue Squad</b><br>Rescue Station 17<br>305 Sandbridge Road<br>Virginia Beach, VA 23456<br>Phone: 385-2917<br><a href="http://sandbridgerescuesquad.com/" target="_blank">http://sandbridgerescuesquad.com/</a>',image,shadow],
				  ['Virginia Beach Office of EMS', -76.06982, 36.82747, 12, '<b>Virginia Beach Office of EMS</b><br>477 Viking Drive Suite 130<br>Virginia Beach, VA 23452<br>Phone: 385-1999<br>Fax: 431-3019<br><a href="http://vabeachems.com/" target="_blank">http://vabeachems.com/</a>',image2,shadow2],
				  ['Virginia Beach EMS Training Center', -75.99325, 36.81135, 13, '<b>Virginia Beach EMS Training Center</b><br>927 South Birdneck Road<br>Virginia Beach, VA 23451<br>Phone: 385-2976<br>Fax: 437-6560<br><a href="http://vabeachems.com/" target="_blank">http://vabeachems.com/</a>',image2,shadow2]];
				var infoPre = '<div id="mapInfoWindow">';
				var infoPost = '</div>';
		
				var infoWindow = new google.maps.InfoWindow({
				    content: '',
					maxWidth: 180
				});
		
				var map = 0;
		
				function initialize() {
					var latlng = new google.maps.LatLng(36.7686, -76.1064);
					var mapOptions = {
						zoom: 10,
						center: latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
			
					map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

					var shape = {
						coord: [1,1,1,31,31,31,31,1],
						type: 'poly'
					};
			
					for(var i=0;i<stations.length;i++) {
						var station = stations[i];
						var stLatLng = new google.maps.LatLng(station[2], station[1]);
						mapPoints[i] = new google.maps.Marker({
							position: stLatLng,
							map: map,
					        shadow: station[6],
							icon: station[5],
							shape: shape,
							title: station[0],
							infoString: infoPre+station[4]+infoPost
						});
				
						google.maps.event.addListener(mapPoints[i],'click',function() {
							showInfo(this);
						});
					}
				}
		
				function showInfo(point) {
					infoWindow.setContent(point.infoString);
					infoWindow.open(map,point);
				}
			</script>
	
		<?php } ?>
		
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		
	</head>
	
<body <?php body_class(); ?> <?php if ( is_page('82') ) echo 'onload="initialize()"'; ?>>
		
		<div class="blogheader">
			<div class="blogwrapper">
				<h1 id="logo"><a href="/index.php">Volunteer Rescue Squads of Virginia Beach</a></h1>
				<div id="nav">
					<ul>
						<li><a href="<?php echo site_url(); ?>/blog">VBRS Blog</a></li>
						<li><a href="<?php echo site_url(); ?>">Back to Main Site</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div id="cta">
			<div class="blogwrapper">
				<h2>Volunteers<br />Saving Lives</h2>
				<p>Every day and night, the men and women of the Volunteer Rescue Squads of Virginia Beach are making a difference and saving lives. Here are their stories and information on how you could become a volunteer</p>
				<p><a href="<?php echo site_url(); ?>/join-us" class="more">Join Today</a></p>
			</div>
		</div>
		
	<div id="container">
