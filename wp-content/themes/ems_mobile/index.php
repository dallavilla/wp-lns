<?php get_header(); ?>

	<?php
		$options = get_option('emsmobile_theme_options');
		if ( ($options['emsmobile_main_punchline'] != "") || ($options['emsmobile_headline'] != "")) {
	?>
		<div id="banner">
			<?php
				if ($options['emsmobile_main_punchline'] != "") echo "<div id=\"banner-headline\">".stripslashes($options['emsmobile_main_punchline'])."</div>";
				if ($options['emsmobile_headline'] != "") echo "<div id=\"banner-secondary\">".stripslashes($options['emsmobile_headline'])."</div>";
			?>
		</div>
	<?php } ?>
	<br />

	<?php if (have_posts()) : ?>
	
		<?php $counter = 0; ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="col3">
				<div id="post-<?php the_ID(); ?>">
					<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

					<div class="entry">
						<?php the_excerpt('Read the rest of this entry &raquo;'); ?>
						<p><a href="<?php the_permalink() ?>">Read more &rarr;</a></p>
					</div>
				</div>
			</div>
			<?php
				$counter++;
				// if ($counter % 3 == 0) echo '<div class="cboth"></div>' ;
			?>
		<?php endwhile; ?>

		<div class="cboth pagination">
			<?php emsmobile_pagenavi(); ?>
		</div>

	<?php else : ?>

		<h1 class="acenter">Not Found</h1>
		<p class="acenter">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>
	
<?php get_sidebar(); ?>

<?php get_footer(); ?>
