<?php

add_action( 'admin_init', 'emsmobile_theme_options_init' );
add_action( 'admin_menu', 'emsmobile_theme_options_add_page' );

/**
 * Init plugin options to white list our options
*/
function emsmobile_theme_options_init(){
	register_setting( 'emsmobile_options', 'emsmobile_theme_options', 'emsmobile_theme_options_validate' );
}

/**
 * Load up the menu page
*/
function emsmobile_theme_options_add_page() {
	add_theme_page( 'EMS Mobile Theme Options', 'EMS Mobile Theme Options', 'edit_theme_options', 'emsmobile_theme_options', 'emsmobile_theme_options_do_page' );
}

/**
 * Create the options page
*/
function emsmobile_theme_options_do_page() {
	global $emsmobile_radio_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . " Theme Options</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<div class="updated fade"><p><strong>Options saved.</strong></p></div>
		<?php endif; ?>

		<form method="post" action="options.php" style="float: left; width: 70%;">
			<?php settings_fields( 'emsmobile_options' ); ?>
			<?php $options = get_option( 'emsmobile_theme_options' ); ?>

			<table class="form-table">
				<tr valign="top">
					<td colspan="2">
						<h2>Home Page Banner</h2>
						<p>Home Page Banner will be hidden if Main Punchline and Headline fields left empty.</p>
					</td>
				</tr>
				<tr valign="top"><th scope="row">Main Punchline:</th>
					<td>
						<input id="emsmobile_theme_options[emsmobile_main_punchline]" class="regular-text" type="text" name="emsmobile_theme_options[emsmobile_main_punchline]" value="<?php esc_attr_e(stripslashes($options['emsmobile_main_punchline'])); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row">Headline:</th>
					<td>
						<textarea id="emsmobile_theme_options[emsmobile_headline]" class="large-text" cols="50" rows="3" name="emsmobile_theme_options[emsmobile_headline]"><?php echo esc_attr(stripslashes($options['emsmobile_headline'])); ?></textarea>
					</td>
				</tr>
			</table>
			
			<table class="form-table">			
				<tr valign="top">
					<td colspan="2">
						<h2>Google Analytics</h2>
						<p>Google Analytics code will NOT be included if Google Analytics ID field left empty.</p>
					</td>
				</tr>
				<tr valign="top"><th scope="row">Google Analytics ID:</th>
					<td>
						<input id="emsmobile_theme_options[ga_code]" class="regular-text" type="text" name="emsmobile_theme_options[ga_code]" value="<?php esc_attr_e(stripslashes($options['ga_code'])); ?>" /><br />
						<label class="description" for="emsmobile_theme_options[ga_code]">Copy and paste your Google Analytics account ID (UA-XXXXXXXX-X) here.</label>
					</td>
				</tr>		
			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="Save Options" />
			</p>
		</form>

<style>

.postbox .hndle {
    cursor: auto;
    font-size: 13px;
    margin: 0;
    padding: 6px 10px 7px;
}
.panel-wrap.inside {
    padding: 0 10px 10px;
}
.panel-wrap.inside ul {
    padding-top: 12px;
}
.panel-wrap.inside ul li {
	font-size: 11px;
    padding-bottom: 6px;
}
</style>
		
		
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
*/
function emsmobile_theme_options_validate( $input ) {
	// Say our textarea option must be safe text with the allowed tags for posts
	$input['emsmobile_main_punchline'] = wp_filter_post_kses( $input['emsmobile_main_punchline'] );
	$input['emsmobile_headline'] = wp_filter_post_kses( $input['emsmobile_headline'] );
	$input['ga_code'] = wp_filter_post_kses( $input['ga_code'] );

	return $input;
}

?>